const fs = require("fs");
const about = require("express").Router();

const history = {
  mindset:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.",
  culture:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.",
  timeline: [
    {
      label: "success",
      year: "2016",
      title:
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below.",
      content:
        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form."
    },
    {
      label: "success",
      year: "2017",
      title:
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below.",
      content:
        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form."
    },
    {
      label: "success",
      year: "2018",
      title:
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below.",
      content:
        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form."
    },
    {
      label: "success",
      year: "2019",
      title:
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below.",
      content:
        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form."
    }
  ]
};

const ambitions = {
  vision: {
    title:
      "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.",
    content:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
  },
  mission: {
    title:
      "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.",
    content:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
  },
  solutions: {
    title:
      "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.",
    content:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
  }
};

const businessMarket = {
  title:
    "There are many variations of passages of Lorem Ipsum available, but the majority have.",
  numbers: [
    {
      value: "2.7",
      unit: "k"
    },
    {
      value: "3.1",
      unit: "kh"
    }
  ],
  projects: [
    {
      title: "Banking",
      content:
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested."
    },
    {
      title: "Banking 1",
      content:
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested."
    }
  ],
  testimonials: [
    {
      avatar:
        "https://cdn.pixabay.com/photo/2016/08/20/05/38/avatar-1606916_960_720.png",
      name: "Simon Dorothy",
      role: "CEO",
      company: {
        logo: "https://www.freelogodesign.org/Content/img/logo-ex-4.png",
        name: "Albel"
      },
      content:
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested."
    },
    {
      avatar:
        "https://cdn.pixabay.com/photo/2016/08/20/05/38/avatar-1606916_960_720.png",
      name: "Simon Dorothy 1",
      role: "CEO",
      company: {
        logo: "https://www.freelogodesign.org/Content/img/logo-ex-4.png",
        name: "Albel"
      },
      content:
        "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested."
    }
  ]
};

/**
 * @route /
 * @method GET
 * @return Root access
 */
about.get("/", (req, res) => {
  res.json({ page: "About" });
});

/**
 * @route /history
 * @method GET
 * @return MAF history & values
 */
about.get("/history", (req, res) => {
  res.json(history);
});

/**
 * @route /ambitions
 * @method GET
 * @return MAF ambitions
 */
about.get("/ambitions", (req, res) => {
  res.json(ambitions);
});

/**
 * @route /business
 * @method GET
 * @return MAF business market
 */
about.get("/business", (req, res) => {
  res.json(businessMarket);
});

/**
 * @route /coop
 * @method POST
 * @description Contact MAF for a business
 * @return message
 */
about.post("/coop", (req, res) => {
  const { phone, fullname } = req.body;

  const file = "db/coop.json";

  fs.readFile(file, "utf8", (err, data) => {
    const db = JSON.parse(data);
    db.subscriptions.push({ phone, fullname });

    fs.writeFile(file, JSON.stringify(db, null, 2), err => {
      if (!err) {
        res.json({ msg: "OK" });
      }
    });
  });
});

module.exports = about;
