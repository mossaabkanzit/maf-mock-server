const upload = require("express").Router();
const fs = require("fs");
const path = require("path");
const multer = require("multer");
const checkFileType = require("../utils/checkFileType");

// Set The Storage Engine
const storage = multer.diskStorage({
  destination: "public/uploads/",
  filename: function(req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  }
});

// Init Upload
const uploader = multer({
  storage: storage,
  limits: { fileSize: 1000000 },
  fileFilter: function(req, file, cb) {
    checkFileType(file, cb);
  }
});

/**
 * @route /upload
 * @method POST
 * @description Upload CV
 * @return Upload status
 */
upload.post("/", uploader.single("cv"), (req, res) => {
  const suffix = req.body.ref;
  const filename = req.file.filename.split(".");

  fs.rename(
    `public/uploads/${req.file.filename}`,
    `public/uploads/${filename[0]}-${suffix}.${filename[1]}`,
    err => {
      if (err) throw err;
      res.json({
        msg: "File Uploaded!",
        file: `uploads/${filename[0]}-${suffix}.${filename[1]}`
      });
    }
  );
});

module.exports = upload;
