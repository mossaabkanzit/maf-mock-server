const fs = require("fs");
const _ = require("lodash");
const quiz = require("express").Router();

const encoding = "utf8";

/**
 * @route /
 * @method GET
 * @return Section title & Quiz data
 */
quiz.get("/", (req, res) => {
  fs.readFile("db/quiz.json", encoding, (err, data) => {
    if (err) {
      throw err;
    }

    const db = JSON.parse(data);

    res.json({
      title:
        "There are many variations of passages of Lorem Ipsum available, but the majority have",
      levels: db.levels,
      questions: db.questions
        .filter(q => q.levelId === 1)
        .map(q => ({
          id: q.id,
          levelId: q.levelId,
          label: q.label,
          options: q.options,
          defaultAnswer: q.defaultAnswer
        }))
    });
  });
});

/**
 * @route /levels/:id
 * @method POST
 * @return Level questions
 */
quiz.post("/levels/:id", (req, res) => {
  const id = parseInt(req.params.id, 10);

  fs.readFile("db/quiz.json", encoding, (err, data) => {
    if (err) {
      throw err;
    }

    const db = JSON.parse(data);
    res.json({
      questions: db.questions
        .filter(q => q.levelId === id)
        .map(q => ({
          id: q.id,
          levelId: q.levelId,
          label: q.label,
          options: q.options,
          defaultAnswer: q.defaultAnswer
        }))
    });
  });
});

/**
 * @route /score
 * @method POST
 * @return Operation status
 */
quiz.post("/score", (req, res) => {
  const { level, answers, email, message } = req.body;

  fs.readFile("db/quiz.json", encoding, (err, data) => {
    if (err) {
      throw err;
    }

    const db = JSON.parse(data);

    const questions = db.questions.map(q => `${q.id},${q.correctAnswer}`);
    const response = answers.map(a => `${a.qId},${a.aId}`);

    const score = questions
      .map((q, idx) => q === response[idx])
      .filter(i => i === true).length;

    fs.readFile("db/scores.json", encoding, (err, data) => {
      if (err) {
        throw err;
      }

      const db = JSON.parse(data);
      db.scores.push({ level, score, email, message });

      fs.writeFile("db/scores.json", JSON.stringify(db, null, 2), err => {
        if (!err) {
          res.json({
            length: questions.length,
            correct: score
          });
        }
      });
    });
  });
});

module.exports = quiz;
