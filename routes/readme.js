const readme = require("express").Router();

const data = {
  description:
    "As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience. This page describes what information they gather, how we use it and why we sometimes need to store these cookies.",
  content:
    "<div><article><h2>What Are Cookies</h2><p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience. This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from being stored however this may downgrade or certain elements of the sites functionality.</p> </article></div>"
};

/**
 * @route /
 * @method GET
 * @return Root access
 */
readme.get("/", (req, res) => {
  res.json({ page: "Readme" });
});

/**
 * @route /cookies
 * @method GET
 * @return Cookies policy
 */
readme.get("/cookies", (req, res) => {
  res.json(data);
});

/**
 * @route /legal
 * @method GET
 * @return Legal Terms
 */
readme.get("/legal", (req, res) => {
  res.json(data);
});

/**
 * @route /privacy
 * @method GET
 * @return Privacy policy
 */
readme.get("/privacy", (req, res) => {
  res.json(data);
});

module.exports = readme;
