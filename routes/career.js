const fs = require("fs");
const career = require("express").Router();

const contact = {
  email: "welcome@maltemafrica.com",
  phone: "+212 5 22 55 56 57/78",
  address: "201, Bd Zerktouni. CASABLANCA",
  consulting: "https://linkedin.com",
  africa: "https://linkedin.com",
  impact: "https://linkedin.com",
  youtube: "https://youtube.com",
  facebook: "https://facebook.com",
  twitter: "https://twitter.com"
};

const values = {
  content: `
	<ul>
		<li><span>00</span>Working With Products</li>
		<li><span>01</span>Extend and Retain theAudience</li>
		<li><span>02</span>Various DevelopmentExpertise</li>
		<li><span>03</span>Create ProductStrategy</li>
		<li><span>04</span>Convert Users to Customers</li>
		<li><span>05</span>Post-project Cooperation</li>
	</ul>
`
};

const categories = {
  items: [
    {
      id: 1,
      label: "Business"
    },
    {
      id: 2,
      label: "Data"
    },
    {
      id: 3,
      label: "Software"
    }
  ]
};

const offers = {
  items: [
    {
      id: 1,
      categoryId: 1,
      published:
        "Fri Jan 25 2019 16:26:54 GMT+0100 (Central European Standard Time)",
      title: "Interactive design consultant",
      description:
        "We use cookies for a variety of reasons detailed below. Unfortunately in most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.",
      ref: "123456",
      city: "Casablanca"
    },
    {
      id: 2,
      categoryId: 3,
      published:
        "Fri Jan 25 2019 16:26:54 GMT+0100 (Central European Standard Time)",
      title: "React developer",
      description:
        "We use cookies for a variety of reasons detailed below. Unfortunately in most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.",
      ref: "123456",
      category: "Software",
      city: "Casablanca"
    },
    {
      id: 3,
      categoryId: 2,
      published:
        "Fri Jan 25 2019 16:26:54 GMT+0100 (Central European Standard Time)",
      title: "BI consultant",
      description:
        "We use cookies for a variety of reasons detailed below. Unfortunately in most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.",
      ref: "123456",
      category: "Data",
      city: "Casablanca"
    }
  ]
};

/**
 * @route /
 * @method GET
 * @return Root access
 */
career.get("/", (req, res) => {
  res.json({ page: "Career" });
});

/**
 * @route /contact
 * @method GET
 * @return Contact infos
 */
career.get("/contact", (req, res) => {
  res.json(contact);
});

/**
 * @route /values
 * @method GET
 * @return MAF Work values
 */
career.get("/values", (req, res) => {
  res.json(values);
});

/**
 * @route /offers
 * @method GET
 * @return Offers
 */
career.get("/offers/:id", (req, res) => {
  const id = parseInt(req.params.id || 1, 10);

  const response = {
    items: offers.items.filter(item => item.categoryId === id)
  };

  res.json(response);
});

/**
 * @route /categories
 * @method GET
 * @return Offers categories
 */
career.get("/categories", (req, res) => {
  res.json(categories);
});

/**
 * @route /newsletter
 * @method POST
 * @return Subscribe to newsletter
 */
career.post("/newsletter", (req, res) => {
  const email = req.body.email;

  const file = "db/newsletter.json";

  fs.readFile(file, "utf8", (err, data) => {
    const db = JSON.parse(data);
    db.subscriptions.push(email);

    fs.writeFile(file, JSON.stringify(db, null, 2), err => {
      if (!err) {
        res.json({ msg: "OK" });
      }
    });
  });
});

module.exports = career;
