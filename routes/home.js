const home = require("express").Router();

const data = {
  title:
    "Whether in life or business, the words of successful and innovative people can be deeply inspiring."
};

home.get("/", (req, res) => {
  res.json(data);
});

module.exports = home;
