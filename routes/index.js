const home = require("./home");
const readme = require("./readme");
const career = require("./career");
const about = require("./about");
const quiz = require("./quiz");
const upload = require("./upload");

module.exports = {
  readme,
  home,
  career,
  about,
  quiz,
  upload
};
