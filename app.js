const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

// Server config
const port = 8000;

// Routes handlers
const { home, about, career, readme, quiz, upload } = require("./routes");

// Create server instance
const app = express();

// Configure middleware
app.use(cors());

// Configure routes
app.use("/home", bodyParser.json(), home);
app.use("/about", bodyParser.json(), about);
app.use("/career", bodyParser.json(), career);
app.use("/readme", bodyParser.json(), readme);
app.use("/quiz", bodyParser.json(), quiz);
app.use("/upload", upload);
app.use("/", (req, res) => res.redirect("/home"));

// Start the server
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
